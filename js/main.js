import { CongViec } from "./model.js";
import { validator } from "./validator.js";
import { kiemTraViec, timCongViec, SapXepCongViec } from "./controller.js";
let batLoading = () => {
  document.getElementById("loading").style.display = "flex";
};
let tatLoading = () => {
  document.getElementById("loading").style.display = "none";
};
let BASE_URL = "https://62f8b755e0564480352bf411.mockapi.io";
let dscv = [];
let renderTableDscv = (dscv) => {
  let contentHTML = "";
  dscv.forEach((item) => {
    let content = `
            <li>
                <span>${item.task}</span>
                <span>
                    <button onclick="suaTrangThai('${item.id}')" class="check"><i class="fa fa-check-circle"></i></button>
                    <button onclick="xoaCongViec('${item.id}')" class="delete"><i class="fa fa-trash-alt"></i></button>
                </span>
            </li>
          `;
    contentHTML += content;
  });
  document.getElementById("todo").innerHTML = contentHTML;
};
let renderTableDscvht = (dscvht) => {
  let contentHTML = "";
  dscvht.forEach((item) => {
    let content = `
            <li>
                <span>${item.task}</span>
                <span>
                    <button onclick="suaTrangThai('${item.id}')" class="check"><i class="fa fa-check-circle"></i></button>
                    <button onclick="xoaCongViec('${item.id}')" class="delete"><i class="fa fa-trash-alt"></i></button>
                </span>
            </li>
          `;
    contentHTML += content;
  });
  document.getElementById("completed").innerHTML = contentHTML;
};
let renderDscv = () => {
  batLoading();
  axios({
    url: `${BASE_URL}/toDoList`,
    method: "GET",
  })
    .then((res) => {
      dscv = res.data;
      renderTableDscv(kiemTraViec.viecChuaLam(dscv));
      renderTableDscvht(kiemTraViec.viecDaLam(dscv));
      console.log("dscv: ", dscv);
      tatLoading();
    })
    .catch((err) => {
      tatLoading();
    });
};
renderDscv();

let themViecMoi = () => {
  let viecMoi = new CongViec(document.getElementById("newTask").value);
  if (validator.kiemTraRong(viecMoi.task) == false) {
    return;
  }
  batLoading();
  axios({
    url: `${BASE_URL}/toDoList`,
    method: "POST",
    data: viecMoi,
  })
    .then((res) => {
      renderDscv();
      tatLoading();
    })
    .catch((err) => {
      tatLoading();
    });
};
window.themViecMoi = themViecMoi;

let xoaCongViec = (id) => {
  batLoading();
  axios({
    url: `${BASE_URL}/toDoList/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      renderDscv();
      tatLoading();
    })
    .catch((err) => {
      tatLoading();
    });
};
window.xoaCongViec = xoaCongViec;

let suaTrangThai = (id) => {
  batLoading();
  let congViec = dscv[timCongViec(dscv, id)];
  congViec.process = !congViec.process;
  axios({
    url: `${BASE_URL}/toDoList/${id}`,
    method: "PUT",
    data: congViec,
  })
    .then((res) => {
      renderDscv();
      tatLoading();
    })
    .catch((err) => {
      tatLoading();
    });
};
window.suaTrangThai = suaTrangThai;

let sapXepAZ = () => {
  let cloneDscv = [...dscv];
  let nhungViecChuaLam = kiemTraViec.viecChuaLam(cloneDscv);
  let nhungViecDaLam = kiemTraViec.viecDaLam(cloneDscv);
  SapXepCongViec.tuAdenZ(nhungViecChuaLam);
  SapXepCongViec.tuAdenZ(nhungViecDaLam);
  renderTableDscv(nhungViecChuaLam);
  renderTableDscvht(nhungViecDaLam);
};
window.sapXepAZ = sapXepAZ;

let sapXepZA = () => {
  let cloneDscv = [...dscv];
  let nhungViecChuaLam = kiemTraViec.viecChuaLam(cloneDscv);
  let nhungViecDaLam = kiemTraViec.viecDaLam(cloneDscv);
  SapXepCongViec.tuZdenA(nhungViecChuaLam);
  SapXepCongViec.tuZdenA(nhungViecDaLam);
  renderTableDscv(nhungViecChuaLam);
  renderTableDscvht(nhungViecDaLam);
};
window.sapXepZA = sapXepZA;
