export let timCongViec = (array, id) => {
  let index = array
    .map(function (e) {
      return e.id;
    })
    .indexOf(id);
  return index;
};

export let kiemTraViec = {
  viecChuaLam: (array) => {
    let nhungViecChuaLam = [];
    array.forEach((element) => {
      if (element.process == false) {
        nhungViecChuaLam.push(element);
      }
    });
    return nhungViecChuaLam;
  },
  viecDaLam: (array) => {
    let nhungViecDaLam = [];
    array.forEach((element) => {
      if (element.process == true) {
        nhungViecDaLam.push(element);
      }
    });
    return nhungViecDaLam;
  },
};

export let SapXepCongViec = {
  tuAdenZ: (array) => {
    array.sort((a, b) => a.task.localeCompare(b.task));
    return array;
  },
  tuZdenA: (array) => {
    array.sort((a, b) => a.task.localeCompare(b.task));
    array.reverse();
    return array;
  },
};
